#MAIR_task1

**Written in Python 3.7**

---

Make sure the directory structure of the data is:   
```
Data   
|___*Containing Directories (e.g. Mar13_S0A0)
    |___*Directories containing the JSON files
        |___label.json
        |___log.json
```

To use simply launch the script from command line with Python 3.*  
To stop using terminate the process, this is currently the only way to interrupt the script short of quitting the command line or exhausting all the dialogues