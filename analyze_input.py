import lstm
from keras.models import model_from_json
import numpy

data = lstm.preparedata('dataset.txt')

try:
    # load json and create model
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    # load weights into new model
    model.load_weights("model.h5")
    print("Loaded model from disk")
    # evaluate loaded model
    x_test = data['test']['sentences']
    y_test = data['test']['labels']
    scores = model.evaluate(x=x_test, y=y_test)
    print("evaluation:")
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))
except IOError as e:
    model = lstm.preparemodel(data['dictionary'])
    x_train = data['train']['sentences']
    y_train = data['train']['labels']
    x_test = data['test']['sentences']
    y_test = data['test']['labels']
    # Fit the model
    model.fit(x_train, y_train,batch_size=32, epochs=5)
    # evaluate model
    scores = model.evaluate(x=x_test, y=y_test)
    print("evaluation:")
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))
    # save the model
    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model.h5")
    print("Saved model to disk")
finally:
    while True:
        inp = input('input phrase for testing\n')
        if inp == 'exit':
            exit(0)
        test = model.predict(lstm.decodeInput(inp, data['dictionary']), verbose=1)
        print(data['dict'][numpy.argmax(test)])

