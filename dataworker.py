import os

class DataWorker:

    def __init__(self, path):
        self.path = path

    def getData(self, filename):
        labels = []
        sentences = []
        file = open(os.path.join(self.path, filename), "r").readlines()
        for line in file:
            splitseq = line.split(" ")
            labels.append(splitseq[1])
            sentences.append(line.replace(splitseq[1], ''))
        # labels = self.uniquify(labels)
        result = {'labels': labels, 'sentences': sentences}
        return result

    def convertLabels(self, index, list):
        output = []
        for element in list:
            output.append(index[element])
        return output
