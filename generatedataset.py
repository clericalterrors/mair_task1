import os
import json
import random

def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]


# gets the files from which to pull the JSON data
def getfiles(path):
    files = []
    for (path, x, filenames) in walklevel(path, 2):
        files.extend(os.path.join(path, name) for name in filenames if 'label.json' in name)
    return files


def readJson(file):
    results = []
    turns = json.load(open(file, "r"))['turns']
    for turn in turns:
        firstpart = turn['semantics']['cam'].split("(")
        thing = firstpart[0] + " " + turn['transcription']
        results.append(thing)

    return results

results = getfiles('data')

filewrite = open("dataset.txt", "w")

toWrite = []
for file in results:
    inputs = readJson(file)
    for line in inputs:
        toWrite.append(line)
random.shuffle(toWrite)
print(len(toWrite))

for line in toWrite:
    filewrite.write(" %s\n" % line)

