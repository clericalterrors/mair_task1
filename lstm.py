from decimal import *

from keras.layers import *
from keras.models import *
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.text import text_to_word_sequence
from keras.utils.np_utils import to_categorical
from tokenizer import tokenize
from dataworker import DataWorker

def decodeInput(input, dictionary):
    bigList = []
    list = []
    inputSplitted = input.split()
    print(inputSplitted)
    for element in inputSplitted:
        if element in dictionary:
            list.append(dictionary[element])
        else:
            list.append(1)
    bigList.append(list)
    return pad_sequences(bigList, maxlen=10, value=0)

def preparedata(file):
    worker = DataWorker('./')
    data = worker.getData(file)
    sentences, dictionary = tokenize(data['sentences'])

    encoded_sentences = pad_sequences(sentences, maxlen=10, value=0)

    tokenizer = Tokenizer(num_words=15)
    tokenizer.fit_on_texts(data['labels'])
    label_index = tokenizer.word_index

    print(label_index)

    labels_converted = worker.convertLabels(label_index, data['labels'])

    encoded_labels = to_categorical(labels_converted)

    print('Shape of data tensor:', encoded_sentences.shape)
    print('Shape of label tensor:', encoded_labels.shape)

    sen_limit = int(round(Decimal(len(encoded_sentences)) * Decimal(0.85)))
    label_limit = int(round(Decimal(len(encoded_labels)) * Decimal(0.85)))

    return {'train': {
        'sentences': encoded_sentences[:sen_limit],
        'labels' : encoded_labels[:label_limit]
    },
    'test': {
        'sentences': encoded_sentences[sen_limit:],
        'labels': encoded_labels[label_limit:]
    },
    'dictionary': dictionary,
    'dict': dict(map(reversed, label_index.items()))}


# Model goes here
def preparemodel(word_index):
    main_input = Input(shape=(10, ), name='main_input')
    x = Embedding(output_dim=512, input_dim=800, input_length=10, mask_zero=True)(main_input)
    x = LSTM(32)(x)

    x = Dense(128, activation='relu')(x)
    x = Dense(128, activation='relu')(x)
    x = Dense(128, activation='relu')(x)

    main_output = Dense(16, activation='softmax', name='main_output')(x)

    model = Model(inputs=[main_input], outputs=[main_output])

    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    return model
