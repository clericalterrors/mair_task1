Experiment with the following parameters: size of the embedding, sequence length, LSTM units, number of Dense layers,
number of units in each Dense layer, alternative activation functions, training batch size.

Please write down what you changed in the new model and paste the results below.

Always make sure that the the shape of the label tensor in the test and training set is the same! (*, 16)
If one of the two sets has (*, 15) then regenerate the datasets and try again.

RESULTS:
1) BASE MODEL: acc = 70% (more or less)

BASE MODEL:
      main_input = Input(shape=(10, ), name='main_input')

      #Model goes here
      x = Embedding(output_dim=100, input_dim=len(word_index_test) + 1, input_length=10, mask_zero=True)(main_input)
      x = LSTM(250)(x)

      x = Dense(200, activation='relu')(x)
      x = Dense(100, activation='relu')(x)
      x = Dense(50, activation='relu')(x)

      main_output = Dense(16, activation='softmax', name='main_output')(x)

      model = Model(inputs=[main_input], outputs=[main_output])


      x_train = encoded_sentences
      y_train = encoded_labels

      x_test = encoded_sentences_test
      y_test = encoded_labels_test

      #Compile model.
      model.compile(loss='categorical_crossentropy',
                    optimizer='rmsprop',
                    metrics=['accuracy'])

      #Fit the model
      model.fit(x_train, y_train,
          batch_size=32,
          epochs=5
      )

      #Evaluate the model
      scores = model.evaluate(
          x=x_test,
          y=y_test
      )

      print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))

1)  Results with BASE MODEL:
      Epoch 1/5
      2018-09-30 22:05:53.999581: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
      21654/21654 [==============================] - 16s 753us/step - loss: 0.4335 - acc: 0.8791
      Epoch 2/5
      21654/21654 [==============================] - 16s 752us/step - loss: 0.1407 - acc: 0.9677
      Epoch 3/5
      21654/21654 [==============================] - 18s 809us/step - loss: 0.1167 - acc: 0.9720
      Epoch 4/5
      21654/21654 [==============================] - 18s 839us/step - loss: 0.1036 - acc: 0.9759
      Epoch 5/5
      21654/21654 [==============================] - 18s 833us/step - loss: 0.0978 - acc: 0.9771
      3847/3847 [==============================] - 1s 252us/step
      acc: 72.47%

      Epoch 1/5
      2018-09-30 22:03:10.873166: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
      21654/21654 [==============================] - 15s 696us/step - loss: 0.4560 - acc: 0.8757
      Epoch 2/5
      21654/21654 [==============================] - 16s 725us/step - loss: 0.1473 - acc: 0.9652
      Epoch 3/5
      21654/21654 [==============================] - 16s 760us/step - loss: 0.1237 - acc: 0.9713
      Epoch 4/5
      21654/21654 [==============================] - 16s 756us/step - loss: 0.1086 - acc: 0.9739
      Epoch 5/5
      21654/21654 [==============================] - 16s 756us/step - loss: 0.1028 - acc: 0.9751
      3847/3847 [==============================] - 1s 239us/step
      acc: 68.11%

      Epoch 1/5
      2018-09-30 22:11:45.899817: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
      21654/21654 [==============================] - 15s 690us/step - loss: 0.4539 - acc: 0.8730
      Epoch 2/5
      21654/21654 [==============================] - 15s 673us/step - loss: 0.1452 - acc: 0.9653
      Epoch 3/5
      21654/21654 [==============================] - 17s 772us/step - loss: 0.1231 - acc: 0.9718
      Epoch 4/5
      21654/21654 [==============================] - 16s 759us/step - loss: 0.1117 - acc: 0.9732
      Epoch 5/5
      21654/21654 [==============================] - 17s 763us/step - loss: 0.1046 - acc: 0.9762
      3847/3847 [==============================] - 1s 241us/step
      acc: 73.33%

      Epoch 1/5
      2018-09-30 22:13:58.925420: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
      21654/21654 [==============================] - 15s 695us/step - loss: 0.4240 - acc: 0.8840
      Epoch 2/5
      21654/21654 [==============================] - 15s 708us/step - loss: 0.1400 - acc: 0.9679
      Epoch 3/5
      21654/21654 [==============================] - 17s 806us/step - loss: 0.1166 - acc: 0.9722
      Epoch 4/5
      21654/21654 [==============================] - 17s 764us/step - loss: 0.1066 - acc: 0.9755
      Epoch 5/5
      21654/21654 [==============================] - 17s 763us/step - loss: 0.0960 - acc: 0.9764
      3847/3847 [==============================] - 1s 239us/step
      acc: 72.45%

2) With output_dim = 512
      Epoch 1/5
      2018-09-30 22:22:01.326573: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
      21654/21654 [==============================] - 34s 2ms/step - loss: 0.3196 - acc: 0.9165
      Epoch 2/5
      21654/21654 [==============================] - 33s 2ms/step - loss: 0.1197 - acc: 0.9717
      Epoch 3/5
      21654/21654 [==============================] - 32s 1ms/step - loss: 0.0988 - acc: 0.9772
      Epoch 4/5
      21654/21654 [==============================] - 34s 2ms/step - loss: 0.0854 - acc: 0.9800
      Epoch 5/5
      21654/21654 [==============================] - 32s 1ms/step - loss: 0.0765 - acc: 0.9826
      3847/3847 [==============================] - 2s 402us/step
      acc: 70.50%

3) With 32 nodes in LSTM
      Epoch 1/5
      2018-09-30 22:21:03.662848: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
      21654/21654 [==============================] - 10s 449us/step - loss: 0.3430 - acc: 0.9086
      Epoch 2/5
      21654/21654 [==============================] - 9s 431us/step - loss: 0.1196 - acc: 0.9721
      Epoch 3/5
      21654/21654 [==============================] - 9s 433us/step - loss: 0.1005 - acc: 0.9753
      Epoch 4/5
      21654/21654 [==============================] - 9s 437us/step - loss: 0.0870 - acc: 0.9794
      Epoch 5/5
      21654/21654 [==============================] - 9s 424us/step - loss: 0.0765 - acc: 0.9814
      3847/3847 [==============================] - 0s 111us/step
      acc: 67.69%

