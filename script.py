import os
import json
from itertools import islice, cycle


# small roundrobin function to help organize the data
def roundrobin(*iterables):
    "roundrobin('ABC', 'D', 'EF') --> A D E B F C"
    # Recipe credited to George Sakkis
    num_active = len(iterables)
    nexts = cycle(iter(it).__next__ for it in iterables)
    while num_active:
        try:
            for next in nexts:
                yield next()
        except StopIteration:
            # Remove the iterator we just exhausted from the cycle.
            num_active -= 1
            nexts = cycle(islice(nexts, num_active))


# returns the information from the JSON files in a way that can be printed
def readJson(paths):
    labels = []
    logs = []
    for file in paths:
        if('label' in file):
            labels.append(json.load(open(file, "r"))["task-information"]["goal"]["text"])
            turns = json.load(open(file, "r"))['turns']
            for turn in turns:
                labels.append("User: " + turn['transcription'])
        else:
            logs.append("[Session-id]: " + json.load(open(file, "r"))["session-id"])
            turns = json.load(open(file, "r"))['turns']
            for turn in turns:
                logs.append("System: " + turn['output']['transcript'])

    conversation = roundrobin(logs, labels)
    printable = list(conversation)
    return printable


# Helps walk through the different folders, small extension of os.walk
def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]


# determines from which folders to pull the JSON files and calls the relevant function
def getfolders(path, indexobject):
    marfolders = []
    foldersagain = []
    internalindex = indexobject.getmarIndex()

    for (path, dirnames, filenames) in walklevel(path, 0):
        marfolders.extend(os.path.join(path, name) for name in dirnames)

    if(internalindex >= len(marfolders)):
        return False

    for (path, dirnames, filenames) in walklevel(marfolders[internalindex], 0):
        foldersagain.extend(os.path.join(path, name) for name in dirnames)
    foldersagainlen = len(foldersagain)

    if indexobject.getNum() >= foldersagainlen:
        indexobject.setNum(0)
        indexobject.incMarIndex()

    return getfiles(foldersagain[indexobject.getNum()])

# gets the files from which to pull the JSON data
def getfiles(path):
    files = []
    for (path, x, filenames) in walklevel(path, 1):
        files.extend(os.path.join(path, name) for name in filenames)
    return readJson(files)

# helps keep track of which folders to pull files from
class GlobalIndex:
    def __init__(self):
        self.num = 0
        self.marIndex = 0

    def getNum(self):
        return self.num

    def setNum(self, value):
        self.num = value

    def increment(self):
        self.num = self.num + 1

    def getmarIndex(self):
        return self.marIndex

    def incMarIndex(self):
        self.marIndex = self.marIndex + 1

# instantiates the index object outside the main loop to make sure the values are independent from the loop itself
globalIn = GlobalIndex()

# the main loop, awaits ENTER to print more dialogue
while True:
    inp = input("Press Enter to print the next dialogue")

    file = open("result.txt", "a")
    if not inp:
        result = getfolders('data', globalIn)
        #exits the script once all dialogues are exhausted
        if not result:
            print("No more dialogues are available, exiting")
            exit()
        for element in result:
            print(element)
            file.write(" %s\n" % element)
        print("------")
        file.write("\n-------\n\n")
        file.flush()
        globalIn.increment()

        inp = False


