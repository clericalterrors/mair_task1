def build_vocab(file):
    dictionary = {}
    count = 2
    for line in file:
        fileSplitted = line.replace("\n", "").split()
        for word in fileSplitted:
            word.lower()
            if word in dictionary:
                pass
            else:
                count += 1
                dictionary[word] = count
    # print(dictionary)

    return dictionary

def file_to_word_ids(file, dictionary):
    bigList = []
    for line in file:
        lineSplitted = line.replace("\n", "").split()
        list = []
        for word in lineSplitted:
            if word in dictionary:
                list.append(dictionary[word])
        bigList.append(list)

    return bigList

def tokenize(file):
    dictionary = build_vocab(file)
    sentences = file_to_word_ids(file, dictionary)
    reversed_dictionary = dict(zip(dictionary.values(), dictionary.keys()))

    return sentences, dictionary